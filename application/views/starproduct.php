
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include('share/title.php'); ?>
<script src="<?= base_url('assets/theme');?>/js/jquery-3.1.1.js"></script>
  <link href="<?= base_url('assets/theme');?>/css/bootstrap.css" rel="stylesheet">
  <link href="<?= base_url('assets/theme');?>/css/style.css" rel="stylesheet">

  <script type="text/javascript">
    $(document).ready(function() {
      hideAll();
	  $('#star').show();

      $('.show-star').click(function() {
        hideAll();
		 $('#star').fadeIn(999);
      });
      
    });

    function hideAll() {
     
    }
  </script>

</head>

<body>
<?php include('share/rsmenu.php');?>
<p><img src="<?= base_url('assets/theme/');?>images/cafe-coffee.jpg" style="width:100%"></p>
<div class="container amor-meaning">
<div class="progress-bar-info">&nbsp;&nbsp;&nbsp;&nbsp;Bakery & Coffee Shop รายการแนะนำ</div>
</div>

<div id="star" class="container" style="display: none;">
      <?php foreach($products as $row): ?>
      <form class="form-inline" action="<?= base_url('Api/get_order');?>">
				<div class="col-sm-6 col-md-4 col-lg-3 product-card-1p">
					<a href="<?= base_url('assets/uploads/product/'.$row['typeProduct'].'/'.$row['images']);?>" data-fancybox="group" data-caption="My caption" data-width="370" data-height="370">
						<img src="<?= base_url('assets/uploads/product/'.$row['typeProduct'].'/'.$row['images']);?>" class="col-xs-12">
					</a>
						<div class="col-md-12 productNameEn"><?= $row['nameEn']?></div>
						<div class="col-md-8 productNameTh"><?= $row['nameTh']?></div>
						<div class="col-sm-3 col-md-3 productPriceBig text-center"><?= $row['cost']?>.-</div>
            <div class="clear"></div>
            <div><button type="submit" class="btn btn-danger" style="width:100%;">Order</button></div>
            <input type="text" id="product_id" name="product_id" value = "<?= $row['id']?>" style="visibility: hidden">

				</div>
      </form>
			<?php endforeach; ?>
</div>
           
            
     <?php include("share/footer.php");?>
</body>
</html>