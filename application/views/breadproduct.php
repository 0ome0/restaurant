
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php include('share/title.php'); ?>
<script src="<?= base_url('assets/theme');?>/js/jquery-3.1.1.js"></script>
  <link href="<?= base_url('assets/theme');?>/css/bootstrap.css" rel="stylesheet">
  <link href="<?= base_url('assets/theme');?>/css/style.css" rel="stylesheet">
  
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

<script type="text/javascript">
    $(document).ready(function() {
      hideAll();
	  $('#bread').show();

     
      $('.show-bread').click(function() {
        hideAll();
        $('#bread').fadeIn(999);
      });
     
    });

    function hideAll() {
    }
  </script>
  
</head>

<body>
<?php include('share/rsmenu.php');?>
 <img src="<?= base_url('assets/theme');?>/images/coffee-vintage.jpg" style="width:100%">
<p>&nbsp;</p>

<div class="container amor-meaning">
  <div class="progress-bar-info">&nbsp;&nbsp;&nbsp;&nbsp;Bakery & Coffee Shop ขนมปัง</div>
  </div>

  <div id="bread" class="container" style="display: none;">
          
      <?php foreach($products as $row): ?>
      <form class="form-inline" action="<?= base_url('Api/get_order');?>">
				<div class="col-sm-6 col-md-4 col-lg-3 product-card-1p">
					<a href="<?= base_url('asset/images/product/bread/'.$row['images']);?>" data-fancybox="group" data-caption="My caption" data-width="370" data-height="370">
						<img src="<?= base_url('assets/images/product/bread/'.$row['images'])?>" class="col-xs-12">
					</a>
						<div class="col-md-12 productNameEn"><?= $row['nameEn']?></div>
						<div class="col-md-8 productNameTh"><?= $row['nameTh']?></div>
						<div class="col-sm-3 col-md-3 productPriceBig text-center"><?= $row['cost']?>.-</div>
            <div class="clear"></div>
            <div><button type="submit" class="btn btn-danger" style="width:100%;">Order</button></div>
            <input type="text" id="product_id" name="product_id" value = "<?= $row['id']?>" style="visibility: hidden">

				</div>
      </form>
			<?php endforeach; ?>

  </div>
</div>
    
     <?php include("share/footer.php");?>
</body>
</html>