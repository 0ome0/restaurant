<script src="<?= base_url('assets/theme');?>/js/jquery-3.1.1.js"></script>
  
  <link href="<?= base_url('assets/theme');?>/css/style.css" rel="stylesheet">
<link href="<?= base_url('assets/theme');?>/css/rsmenu.css" rel="stylesheet" type="text/css">


<nav>

<div class="navbar navbar-inverse navbar-fixed-top amor-nav-bar">


  <div id="logo" ><img src="<?= base_url('assets/theme');?>/images/logocoffee.png" width="129" height="114" >Bakery & Coffee</div>

        <label for="drop" class="toggle">Menu</label>
        <input type="checkbox" id="drop" />
            <ul class="menu">
                <li><a href="<?= base_url('product');?>">หน้าแรก</a></li>
                <li>
                    <!-- First Tier Drop Down -->
                    <label for="drop-1" class="toggle">เบเกอรี่ +</label>
                    <a href="#">เบเกอรี่</a>
                    <input type="checkbox" id="drop-1"/>
                    <ul>
                        <li><a href="<?= base_url('product/breadproduct');?>">ขนมปัง</a></li>
                        <li><a href="<?= base_url('product/cakeproduct');?>">ขนมเค๊ก</a></li>
                        
                    </ul> 

                </li>
                <li>

                <!-- First Tier Drop Down -->
                <label for="drop-2" class="toggle">เครื่องดื่ม+</label>
                <a href="#">เครื่องดื่ม</a>
                <input type="checkbox" id="drop-2"/>
                <ul>
                    
                    <li><a href="#">ชา</a></li>
                    <li>
                       
                    <!-- Second Tier Drop Down -->        
                    <label for="drop-3" class="toggle">กาแฟ +</label>
                    <a href="#">กาแฟ</a>         
                    <input type="checkbox" id="drop-3"/>

                    <ul>
                        
                        <li><a href="#">อาราปิก้า</a></li>
                        <li><a href="#">อุปกรณ์กาแฟ</a></li>
                    </ul>
                    </li>
                </ul>
                </li>
                <li><a href="#">ติดต่อเรา</a></li>
               
                
            </ul>
        
</nav>
