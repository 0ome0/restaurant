<footer>
  <div class="amor-footer-logo">
    <div class="container">
      <div class="" style="width:20%;">
        <img src="<?= base_url('assets/theme');?>/images/logocoffee.png" width="99" height="99" class="logo">
      </div>
    </div>
  </div>
  <div class="amor-footer-copyright">
    <div class="container">
      <p>เบเกอรี่ & คอฟฟี่ ©2018 All Rights Reserved.</p>
    </div>
  </div>
</footer>
