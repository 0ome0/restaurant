<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_list extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{   

        date_default_timezone_set("Asia/Bangkok");
        
        $sql = "select * from t_food_menu";
        $menu_list= $this->db->query($sql);
        $data['menu_list'] = $menu_list->result_array();

        

        $this->load->view('menu_list_view',$data);
    }
    
    public function ordered(){
        date_default_timezone_set("Asia/Bangkok");

        $this->load->view('ordered_view.php') ;
    }
}
