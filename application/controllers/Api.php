<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    public function __construct()
	{
		parent::__construct();

		$this->load->model('Product_model', 'Product_model');
	}

	public function index()
	{
		$this->load->view('welcome_message');
    }
    
    public function get_order(){

        date_default_timezone_set("Asia/Bangkok");

        $product_id = $this->input->get('product_id');

        $products = $this->Product_model->get_product_by_id($product_id);

        $nameTh = $products[0]['nameTh'];
        $nameEn = $products[0]['nameEn'];
        $typeProduct = $products[0]['typeProduct'];
        $cost = $products[0]['cost'];

        $key = $this->Product_model->get_token();
        $Token = $key[0]['token_key'];

        $message = "มี Order : $typeProduct - $nameTh";

        // echo $message;

        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms =  trim($message); // ข้อความที่ต้องการส่ง
        // date_default_timezone_set("Asia/Bangkok");
        $chOne = curl_init(); 
        curl_setopt( $chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify"); 
        // SSL USE 
        curl_setopt( $chOne, CURLOPT_SSL_VERIFYHOST, 0); 
        curl_setopt( $chOne, CURLOPT_SSL_VERIFYPEER, 0); 
        //POST 
        curl_setopt( $chOne, CURLOPT_POST, 1); 
        curl_setopt( $chOne, CURLOPT_POSTFIELDS, "message=$mms"); 
        curl_setopt( $chOne, CURLOPT_FOLLOWLOCATION, 1); 
        $headers = array( 'Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$lineapi.'', );
            curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers); 
        curl_setopt( $chOne, CURLOPT_RETURNTRANSFER, 1); 
        $result = curl_exec( $chOne ); 
        //Check error 
        if(curl_error($chOne)) 
        { 
            echo 'error:' . curl_error($chOne); 
        } 
        else { 
            $result_ = json_decode($result, true); 
            redirect('/menu_list/ordered','refresh');
        } 
        curl_close( $chOne );

    }


    public function ordered(){

        $this->load->view('ordered_view.php') ;
    }

}
