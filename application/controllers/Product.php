<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Product_model', 'Product_model');
	}

	
	public function index(){

		redirect('/Product/starproduct','refresh');

	}

	public function breadproduct() {
	
		$typeProduct = "bread";

		$data['products'] = $this->Product_model->get_product_by_type($typeProduct);

		$this->load->view('breadproduct',$data);
	
	}

	public function cakeproduct() {

		$typeProduct = "cakepiece";

		$data['products'] = $this->Product_model->get_product_by_type($typeProduct);

		$this->load->view('cakeproduct',$data);

	}

	public function starproduct() {

		$data['products'] = $this->Product_model->get_start_product();

		// $this->load->view('starproduct',$data);
		$this->load->view('starproduct',$data);

	}
}
